﻿using System;
namespace org.hpcshelf.exception
{
    public class ComponentNotFoundException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.COMPONENT_NOT_FOUND_EXCEPTION.ToString();

        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public ComponentNotFoundException(string component_id, string system_id) : base(component_id, system_id, String.Format("A component named {0} does not exist in {1} system", component_id, system_id))
        {
        }

    }
}
