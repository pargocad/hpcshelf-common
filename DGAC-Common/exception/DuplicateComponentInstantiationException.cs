﻿using System;
namespace org.hpcshelf.exception
{
    public class DuplicateComponentInstantiationException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.DUPLICATE_COMPONENT_INSTANTIATION_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public DuplicateComponentInstantiationException(string component_id, string system_id) : base(component_id, system_id, String.Format("The component named {0} of system {1} must be released before to be instantiated again", component_id, system_id))
        {
        }
    }
}
