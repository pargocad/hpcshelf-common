﻿using System;
namespace org.hpcshelf.exception
{
    public class EmptyPlatformException : HPCShelfException
    {
        public EmptyPlatformException(string platform_id, string system_id) : base(platform_id, system_id, String.Format("The platform {0} does not have computation components placed on it", platform_id))
        {
        }

        public string PlatformID { get { return ComponentID; } }

        public override string ErrorNumber => ErrorCodes.EMPTY_PLATFORM_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;
    }
}
