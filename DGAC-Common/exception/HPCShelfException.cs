﻿using System;
using static org.hpcshelf.exception.WebServicesException;

namespace org.hpcshelf.exception
{
    public abstract class HPCShelfException : ApplicationException
    {
        private string component_id;
        private string system_id;

        public HPCShelfException(string component_id, string system_id, string message) : base(message)
        {
            this.component_id = component_id;
            this.system_id = system_id;
        }

        public HPCShelfException(string component_id, string message) : base(message)
        {
            this.component_id = component_id;
        }

        public string ComponentID { get { return component_id; } }
        public string SystemID { get  { return system_id; } }
        public abstract string ErrorNumber { get; }
        public abstract FaultCode FaultCode { get; }

    }
}
