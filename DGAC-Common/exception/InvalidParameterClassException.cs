﻿using System;
namespace org.hpcshelf.exception
{
    public class InvalidParameterClassException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.INVALID_PARAMETER_CLASS_EXCEPTION.ToString();

        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public InvalidParameterClassException(string parameter_class, string system_id) : base(parameter_class, system_id, String.Format("A parameter class named {0} does not exist", parameter_class, system_id))
        {
        }
    }
}
