﻿using System;
namespace org.hpcshelf.exception
{
    public class UninstantiatedComponentException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.UNINSTANTIATED_COMPONENT_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public UninstantiatedComponentException(string component_id, string system_id) : base(component_id, system_id, String.Format("The component named {0} of system {1} has not been instantiated yet", component_id, system_id))
        {
        }
    }
}
