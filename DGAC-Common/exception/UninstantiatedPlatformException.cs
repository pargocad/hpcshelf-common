﻿using System;
namespace org.hpcshelf.exception
{
    public class UninstantiatedPlatformException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.UNINSTANTIATED_PLATFORM_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public UninstantiatedPlatformException(string component_id) : base(component_id, String.Format("The component named {0} is not instantiated", component_id))
        {
        }
    }
}
