﻿using System;
namespace org.hpcshelf.exception
{
    public class UnresolvedComponentException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.UNDEPLOYED_COMPONENT_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        public UnresolvedComponentException(string component_id, string system_id) : base(component_id, system_id, String.Format("The component named {0} of system {1} has not been resolved yet", component_id, system_id))
        {
        }
    }
}
