﻿using System;
namespace org.hpcshelf.exception
{
    public class UnresolvedSystemComputationException : HPCShelfException
    {
        public override string ErrorNumber => ErrorCodes.UNRESOLVED_SYSTEM_COMPUTATION_EXCEPTION.ToString();
        public override WebServicesException.FaultCode FaultCode => WebServicesException.FaultCode.Client;

        private string platform_id;

        public UnresolvedSystemComputationException(string computation_id, string platform_id, string system_id) : base(computation_id, system_id, String.Format("No component was found for computation {0} in platform {1}", computation_id, platform_id))
        {
            this.platform_id = platform_id;
        }

        public string PlatformID { get { return platform_id; } }
    }
}
